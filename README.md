# IPTABLES

A bit of helper to configure IP rules with minimal changes on any machine.

This assumes that [iptables](https://www.netfilter.org/projects/iptables/index.html) and [ipset](http://ipset.netfilter.org/) are installed.
There are also bits in there for [crowdsec](https://crowdsec.net) and [fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page) to support intrusion detection and mitigation.
Some basic understanding of each is also assumed, but at the very least they must be installed. 
Installation and configuration of these are out of scope.
Optionally, remove the references in the scripts.

Two scripts are added for ease, and default rules files for both IPv4 and IPv6. 
An example is provided for adding whitelisted or wanted IPs.

## Scripts

The `iptables-fix` script will reset the firewall with the provided rules and call the `iptables-refresh` script to refresh the whitelist and blacklist.

The `iptables-fix` script should be called each time the system restarts to ensure the rules are in place. 
Other mechanisms to persist iptables rules also exist and can be used instead.
Specifics of reloading firewall rules on restart are out of scope.

The scripts are separate so that the whitelist and blacklists can be refreshed without resetting the other rules. 
The `iptables-refresh` can be called independently at any time after `iptables-fix` has been called.

The scripts must be run as the superuser because of the iptables and ipset permissions, unless other configuration changes are made.

### Countries

The list of blacklist IPs comes from a list maintained at [ipdeny.com](http://ipdeny.com)
The set has been chosen based on the [Spamhaus](https://www.spamhaus.org/statistics/countries/) "worst countries" list, then edited to allow my country. 
Originally the rules were created to avoid only incoming e-mail, but it seemed all attacks on the servers came from these countries (and my own). 
This grand blocking, plus the rules in fail2ban, stops most of the attempts to violate the server.

Note that this does also block web traffic to those countries as well.
Using a CDN such as [cloudflare](https://cloudflare.com) will allow delivering web pages from their servers while blocking direct access from computers in untrusted countries.

## Rules

The `iptables.up.rules` file is the core file. 
It is simply configured to allow the ipset whitelist, block the black list, stop some known bad IP attacks, and leave open the few service ports the server may be serving.

In particular, the final bit of the first block should be reviewed and the necessary ports adjusted. 

For example, the following rules blocks all SSH access not allowed by previous rules (the whitelist), and allows mail (SMTP, POPD, IMAP), DNS, and web (HTTP and HTTPS) traffic. 
It's probably safe to leave these here, as it can be assumed that if these services aren't enabled, nothing will listen to those ports anyway. 

Configure these lines as desired.

```
# Hide SSH from un-trusted (pick one)
-A INPUT -p tcp -m tcp --dport 22 -j DROP

# Allow SSH from un-trusted (pick one)
# -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT

# Allow services (DNS, SMTP/IMAP/POP, HTTP(S), etc)
-A INPUT -p tcp -m tcp -m multiport -j ACCEPT --dports 25,53,465,587,80,443,110,143,993,995
-A INPUT -p udp -m udp --dport 53 -j ACCEPT
```

These are at the end of the block, as iptables rules are followed in order.

As written, the whitelist blocks should happen first, so other things can't accidentally block them.
A bevy of invalid packet types checks are done, as they shouldn't be allowed either.
Then the desired services and other rules follow..

### Countries by port

Mentioned above, the ipset rules in the `iptables.up.rules` file blocks all the ports for any IP in the unwanted list. 
This can be changed to block just some ports by modifying the lines to match this SMTP blocking example.

```
iptables -A INPUT -m set --match-set UNWANTED src -p tcp --dport 25 -j DROP
```

## Trusted Networks

For ease, trusted networks are added to a whitelist managed by ipset, by reading any optional files named ending in `.ips` or `.ip6s`, such as the example `wanted.ips` which lists the privite IPs, in the same directory as the rest of these scripts. 
These lists are constructed in the same format as the zone files from [IPDeny](https://www.ipdeny.com/).
Add any whitelisted networks in the CIDR format as desired.

Here's an example allowing traffic from all private IP networks.

```
10.0.0.0/8
172.16.0.0/12
192.168.0.0/16
```

Note that this file is only parsed if it exists. 

## Alternative Rules

A broader set of rules found on https://gist.github.com/jirutka/3742890 are also provided. 
The files 'rules-ipv4.iptables' and 'rules-ipv6.tables' provide a broader set of rules built with community feedback.

These have been left here unaltered, so if there's a need to allow traffic to other ports, change them accordingly.
